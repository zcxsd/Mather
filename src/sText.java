
public class sText {
    //冒泡排序 稳定
//    public static void  main(String[] args){
//        int[] array ={88,34,99,12,56,34,34,65,12};
    //外循环控制要交换多少轮
//        for(int i=0;i<array.length-1;i++){
    //内循环控制每轮交换次数
//            for(int j=0;j<array.length-1-i;j++){
//                if(array[j]>array[j+1]){
//                    array[j]=array[j]+array[j+1];
//                    array[j+1]=array[j]-array[j+1];
//                    array[j]=array[j]-array[j+1];
//                    //System.out.println(array[i]+"");
//                }
//            }
//        }
    //输出排序后的数组
//        for(int i=0;i<array.length;i++){
//            System.out.print(array[i]+" ");
//        }
//    }
    //选择排序算法 不稳定
//    public static  void main(String[] args){
//        int[] array ={88,34,99,12,56,34,34,65,12};
//        int mainmath=0;
//        //外循环控制要交换多少轮
//        for(int i=0;i<array.length-1;i++){
//            mainmath=i;
//            //内循环交换下标
//            for(int j=i+1;j<array.length;j++){
//                if(array[mainmath]>array[j]){
//                    mainmath=j;
//                }
//            }
//            //判断下标是否与自己相等，不相等交换值
//            if(mainmath!=i){
//               array[mainmath]=array[mainmath]+array[i];
//               array[i]=array[mainmath]-array[i];
//               array[mainmath]=array[mainmath]-array[i];
//            }
//        }
//        for(int i=0;i<array.length;i++){
//            System.out.print(array[i]+" ");
//        }
//
//    }
    //插入排序
//    public static  void main(String[] args){
//      int[] array ={88,34,99,12,56,34,34,65,12};
//      int snum=0;
//      //控制插入的轮数
//      for(int i=1;i<array.length;i++){
//          int j=0;
//          snum=array[i];
//          //一轮插入需要交换
//          for(j=i-1;j>=0;j--){
//              if(array[j]>snum){
//                  array[j+1]=array[j];//交换
//              }else{
//                  break;
//              }
//          }
//          if(array[j+1]!=snum){
//              array[j+1]=snum;//相等就不交换
//          }
//      }
//        for(int i=0;i<array.length;i++){
//            System.out.print(array[i]+" ");//输出交换结果
//        }
//    }
    public static void main(String[] args) {
        int[] array = {88, 34, 99, 12, 56, 67, 55, 65, 12};
        int snum=0;
      //控制插入的轮数
      for(int i=1;i<array.length;i++){
          int j=0;
          snum=array[i];
          //一轮插入需要交换
          for(j=i-1;j>=0;j--){
              if(array[j]>snum){
                  array[j+1]=array[j];//交换
              }else{
                  break;
              }
          }
          if(array[j+1]!=snum){
              array[j+1]=snum;//相等就不交换
          }
      }
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");//输出交换结果
        }
        int date=inserts(array,123)+1;
        if(date==0){
            System.out.println("没有你想要查找的数");
        }else
         System.out.println("你想要查找的数在第"+date+"个。");
        //int date=inserts(array,99)+1;
        //System.out.println(date);
    }
    public static int inserts(int array[], int date) {
        int start = 0;
        int end=array.length-1;
        while (start<=end){
            int zj=(start+end)/2;
            if(array[zj]>date){
                end=zj-1;
            }else  if(array[zj]<date){
                start=zj+1;
            }else
                return zj;
        }
        return -1;

    }

}

